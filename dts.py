# Brian Lee (brianlee3742@gmail.com)
import csv
import datetime
import itertools
import operator
import re

from package import PackageList, Package
from status import Status
from truck import Truck


class Dts:
    """
    DTS delivery tracking system.

    Attributes:
        trucks: A dict of the trucks available for deliveries.
        packages: The full PackageList of Package objects.
    """

    NUM_TRUCKS = 2
    DAY_START = datetime.datetime.combine(
        datetime.date.today(), datetime.time(hour=8)
    )
    DAY_END = datetime.datetime.combine(
        datetime.date.today(), datetime.time(hour=17)
    )

    # __init__: O(n²)
    def __init__(self, package_file, distance_file):
        """
        Initializes the DTS delivery system, and loads the package
        and delivery data.

        Args:
            package_file: The filename of the csv package data file,
                as a str.
            distance_file: The csv distance file filename, as a str.
        """
        self.trucks = {n: Truck(n) for n in range(1, Dts.NUM_TRUCKS + 1)}
        for t in self.trucks.values():
            t.current_time = Dts.DAY_START
            t.departure_times[0] = Dts.DAY_START
        self.packages = PackageList()
        self._grouped_packages = []
        self._distance_table = {}
        self._load_package_data(package_file)
        self._load_distance_data(distance_file)

    # _load_package_data: O(n)
    def _load_package_data(self, package_file):
        """
        Loads and parses the package data.

        The package data file must be a csv file formatted as follows:
        id, address, city, state, zip, deadline, weight, special
        instructions.  This will create a new Package for each entry,
        add it to the full package list, and, if indicated by the
        special instructions, add it to the grouped packages dict (with
        a list of packages it must be delivered with) or the truck it
        must be loaded on. It also sets the relevant timestamps for
        each package.

        Args:
            package_file: The name, as a string, of the csv file
                holding package data.
        """
        Package.current_time = Dts.DAY_START
        with open(package_file, newline="") as package_file:
            csv_reader = csv.reader(package_file)
            for row in csv_reader:
                # Normalize the address.
                for full, abbreviation in zip(
                    ("North", "East", "South", "West"), ("N", "E", "S", "W")
                ):
                    row[1] = row[1].replace(full, abbreviation)
                new_package = Package(
                    int(row[0]),
                    row[1],
                    row[2],
                    row[3],
                    row[4],
                    Dts.parse_time(row[5]),
                    row[6],
                )
                # Assume all packages are in the hub at the start of the
                # day until we find out otherwise from the special
                # instructions.
                new_package.timestamps[Status.IN_HUB] = Dts.DAY_START
                self.packages.insert(new_package)
                special_instructions = row[-1]
                # Does this package have to be on a certain truck?
                for truck_number in self.trucks:
                    if f"truck {truck_number}" in special_instructions:
                        new_package.truck = int(truck_number)
                        break
                # Is this package delayed?
                if special_instructions.startswith("Delayed"):
                    delayed_until_time = Dts.parse_time(special_instructions)
                    new_package.timestamps[Status.DELAYED] = Dts.DAY_START
                    new_package.timestamps[Status.IN_HUB] = delayed_until_time
                # Or does this package have the wrong address listed?
                # (Special case: there is only one package with the
                # wrong address listed; it will be fixed at 10:20 AM)
                elif special_instructions.startswith("Wrong address"):
                    new_package.address = "410 S State St"
                    new_package.city = "Salt Lake City"
                    new_package.state = "UT"
                    new_package.zip_code = "84111"
                    new_package.timestamps[Status.DELAYED] = Dts.DAY_START
                    new_package.timestamps[
                        Status.IN_HUB
                    ] = datetime.datetime.combine(
                        datetime.date.today(),
                        datetime.time(hour=10, minute=20),
                    )
                # Does this package have to be delivered with certain
                # other packages?
                if "delivered with" in special_instructions:
                    grouped_ids = {new_package.package_id} | {
                        int(package_id)
                        for package_id in re.findall(
                            r"\d+", special_instructions
                        )
                    }
                    for group in self._grouped_packages:
                        if not grouped_ids.isdisjoint(group):
                            group.update(grouped_ids)
                            break
                    else:
                        self._grouped_packages.append(grouped_ids)

    # _load_distance_data: O(n²)
    def _load_distance_data(self, distance_file):
        """
        Loads and parses the distance data.

        The distance data file must be a csv file whose entries have
        the following fields (in order):
        - The full name of the location
        - The address of the location, either "HUB" (a special case) or
          formatted as "{street address}\n({zip code})"
        - Any number of floats indicating the distance from this
          location to the nth entry's location

        Args:
            distance_file: The name, as a string, of the csv file
                holding distance data.
        """
        with open(distance_file, newline="") as distance_file:
            csv_reader = csv.reader(distance_file)
            for row in csv_reader:
                # Normalize the address.
                address = row[1].strip()
                for full, abbreviation in zip(
                    ("North", "East", "South", "West"), ("N", "E", "S", "W")
                ):
                    address = address.replace(full, abbreviation)
                self._distance_table[address] = {}
                for distance, location in zip(
                    (float(item) for item in row[2:] if item),
                    self._distance_table,
                ):
                    self._distance_table[address][location] = distance
                    self._distance_table[location][address] = distance

    # set_delivery_schedules: O(n)
    def set_delivery_schedules(self):
        """
        Sets the delivery schedules for the trucks.
        """
        trucks = itertools.cycle(self.trucks.values())
        current_truck = next(trucks)
        # Put each group of packages that must be delivered together in
        # its own run.
        for group in self._grouped_packages:
            while not current_truck.current_run_empty():
                current_truck.add_run()
                current_truck = next(trucks)
            for pkg_id in group:
                pkg = self.packages.get(pkg_id)
                if pkg.timestamps[Status.IN_HUB] >= current_truck.current_time:
                    current_truck.current_time = pkg.timestamps[Status.IN_HUB]
                    current_truck.departure_times[
                        current_truck.current_run
                    ] = current_truck.current_time
                current_truck.add_package(pkg)
            current_truck.add_run()
            current_truck = next(trucks)
        # Schedule packages with an early (not "EOD") deadline.
        # Packages with an early deadline that are not in the hub at
        # the start of the day will be processed later so they will
        # not delay the delivery of other packages.
        early_deliveries = sorted(
            (
                pkg
                for pkg in self.packages.packages_not_loaded
                if pkg.deadline != Dts.DAY_END
                and pkg not in self.packages.delayed_packages
            ),
            key=operator.attrgetter("deadline"),
        )
        for _, group in itertools.groupby(
            early_deliveries, operator.attrgetter("deadline")
        ):
            for pkg in group:
                # Does the package have to be on a particular truck?
                if pkg.truck:
                    while pkg.truck != current_truck.number:
                        current_truck = next(trucks)
                if pkg.timestamps[Status.IN_HUB] >= current_truck.current_time:
                    current_truck.current_time = pkg.timestamps[Status.IN_HUB]
                    current_truck.departure_times[
                        current_truck.current_run
                    ] = current_truck.current_time
                current_truck.add_package(pkg)
            while not current_truck.current_run_empty():
                current_truck.add_run()
                current_truck = next(trucks)
        # Schedule the rest of the packages. Packages with an early
        # delivery deadline (before "EOD") will be scheduled first.
        for pkg in sorted(
            self.packages.packages_not_loaded,
            key=operator.attrgetter("deadline"),
        ):
            while current_truck.current_run_full():
                current_truck.add_run()
                current_truck = next(trucks)
            if pkg.truck:
                while pkg.truck != current_truck.number:
                    current_truck = next(trucks)
            if pkg.timestamps[Status.IN_HUB] >= current_truck.current_time:
                current_truck.current_time = pkg.timestamps[Status.IN_HUB]
                current_truck.departure_times[
                    current_truck.current_run
                ] = current_truck.current_time
            current_truck.add_package(pkg)
        for truck in self.trucks.values():
            truck.optimize_delivery_runs(
                distance_function=self.distance_between
            )

    # run_deliveries: O(n)
    def run_deliveries(self):
        """
        Run the deliveries for all trucks.
        """
        for current_truck in self.trucks.values():
            current_truck.current_time = current_truck.departure_times[0]
            for run_number, run in enumerate(current_truck.delivery_runs):
                current_truck.current_run = run_number
                # Travel back to the hub to start this run. (If the
                # truck is already at the hub this will simply add
                # zero miles and zero time.)
                miles_to_hub = self.distance_between(
                    current_truck.current_location, "HUB"
                )
                current_truck.miles_travelled += miles_to_hub
                current_truck.current_time += datetime.timedelta(
                    hours=(miles_to_hub / Truck.SPEED)
                )
                current_truck.current_location = "HUB"
                # If the truck got back to the hub before all of the
                # next group of packages is ready, wait until they can
                # be loaded. Otherwise, make sure the log of departure
                # times accurately reflects this run's start time.
                if (
                    current_truck.current_time
                    < current_truck.departure_times[current_truck.current_run]
                ):
                    current_truck.current_time = current_truck.departure_times[
                        current_truck.current_run
                    ]
                else:
                    current_truck.departure_times[
                        current_truck.current_run
                    ] = current_truck.current_time
                for pkg in run:
                    pkg.timestamps[
                        Status.LOADED
                    ] = current_truck.departure_times[
                        current_truck.current_run
                    ]
                    pkg.timestamps[
                        Status.EN_ROUTE
                    ] = current_truck.departure_times[
                        current_truck.current_run
                    ]
                    delivery_location = pkg.formatted_address
                    miles = self.distance_between(
                        current_truck.current_location, delivery_location
                    )
                    travel_time = datetime.timedelta(
                        hours=(miles / Truck.SPEED)
                    )
                    current_truck.miles_travelled += miles
                    current_truck.current_time += travel_time
                    pkg.timestamps[
                        Status.DELIVERED
                    ] = current_truck.current_time
                    current_truck.current_location = delivery_location
            current_truck.finish_time = current_truck.current_time

    # distance_between: O(1)
    def distance_between(self, start, end):
        """
        Finds the distance between two locations.

        The address of the locations should be either "HUB" (a special
        case) or formatted as "{street address}\n({zip code})".

        Args:
            start: The start location.
            end: The end location.

        Returns: The distance in miles as a float.
        """
        return self._distance_table[start][end]

    # parse_time: O(1)
    @staticmethod
    def parse_time(time_string):
        """
        Converts a textual representation of a time to a datetime.time object.

        Args:
            time_string: A string representation of the time.  It can
                be 12- or 24-hour format, with or without minutes or
                the AM/PM designation.
                Examples: 14:32, 3pm, 4:15 AM, 05:30 AM
        Returns:
            A datetime.time object representing the given time.
        Raises:
            AttributeError: If the string is improperly formatted,
                including a 24-hour hour with 'pm' (E.g. 14:12 PM)
        """
        time_pattern = r"(?P<hour>\d?\d):?(?P<minute>\d\d)?(?::\d\d)? ?(?P<ampm>[AaPp][Mm])?"
        try:
            hour, minute, ampm = re.search(time_pattern, time_string).groups()
        except AttributeError:
            if time_string == "EOD":
                return Dts.DAY_END
            else:
                raise
        hour = int(hour)
        minute = int(minute) if minute else 0
        if ampm:
            if "p" in ampm.lower() and hour != 12:
                hour += 12
            elif "a" in ampm.lower() and hour == 12:
                hour = 0
        if hour not in range(24) or minute not in range(60):
            raise AttributeError
        return datetime.datetime.combine(
            datetime.date.today(), datetime.time(hour=hour, minute=minute)
        )
