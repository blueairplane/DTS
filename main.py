# Brian Lee (brianlee3742@gmail.com)
import enum

from package import Package
from dts import Dts

# Overall complexity: O(n²)
if __name__ == "__main__":
    # Initialization:
    daily_local_deliveries = Dts(
        "DTS Package File.csv", "DTS Distance Table.csv"
    )
    current_time = Dts.DAY_START
    Package.current_time = current_time
    daily_local_deliveries.set_delivery_schedules()
    daily_local_deliveries.run_deliveries()
    # Set up menu:
    Selection = enum.Enum(
        "Selection",
        (
            ("SET_TIME", "Set the current time."),
            ("VIEW_ALL", "View the statuses of all packages."),
            ("VIEW_ONE", "View the status of an individual package."),
            ("VIEW_TRUCK", "View the packages on a particular truck."),
            ("QUIT", "Quit."),
        ),
    )
    options = {
        str(i): selection for i, selection in enumerate(Selection, start=1)
    }
    selection = None
    while selection is not Selection.QUIT:
        print()
        print("DTS DLD Tracking System")
        for option, text in options.items():
            print(f"\t{option}: {text.value}")
        print(f"Current time is {current_time:%I:%M %p}")
        total_miles = sum(
            t.miles_travelled for t in daily_local_deliveries.trucks.values()
        )
        print(f"Total miles travelled: {total_miles}")
        print(daily_local_deliveries.packages.on_time_stats)
        print()
        selection = options.get(
            input(f"Please enter your selection (1 - {max(options)}): ")
        )
        if not selection:
            print("Invalid selection.")
        else:
            print()
            if selection is Selection.SET_TIME:
                try:
                    user_input = input("Please enter the desired time: ")
                    current_time = Dts.parse_time(user_input)
                    Package.current_time = current_time
                except AttributeError:
                    print("Invalid time format.")
            elif selection is Selection.VIEW_ALL:
                print(daily_local_deliveries.packages)
            elif selection is Selection.VIEW_ONE:
                try:
                    package_id = int(input("Please enter the package ID: "))
                    print(daily_local_deliveries.packages.get(package_id))
                except (ValueError, KeyError):
                    print("Invalid package ID.")
            elif selection is Selection.VIEW_TRUCK:
                numbers = " or ".join(
                    ", ".join(
                        str(n) for n in range(1, 1 + Dts.NUM_TRUCKS)
                    ).rsplit(", ")
                )
                truck_number = input(
                    f"Please enter the truck number ({numbers}): "
                )
                if truck_number in (
                    str(n) for n in range(1, 1 + Dts.NUM_TRUCKS)
                ):
                    print(daily_local_deliveries.trucks[int(truck_number)])
                else:
                    print("Invalid truck number.")
