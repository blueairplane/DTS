# Brian Lee (brianlee3742@gmail.com)
from status import Status


class Package:
    """
    A single package for delivery.

    Class attributes:
        current_time: A datetime object representing the current,
            working time for all packages. It is used to determine
            where in the delivery process each package is at a
            given time.

    Attributes:
        package_id: A unique integer ID.
        address: The delivery street address as a str.
        city: The delivery city as a str.
        state: The delivery state as a str.
        zip_code: The delivery zip code as a str.
        deadline: The latest allowable delivery time (formatted
            HH:MM:SS XM) or "EOD" if the package can be delivered at
            any time during the day.
        weight: The package's weight in kg as a str.
        timestamps: A dict of datetime object timestamps for each of
            the possible statuses of the package, or None if that
            status does not apply or has not yet been recorded.
            See Also: status.Status
        truck: The int ID of the truck this package is loaded on.

    Properties:
        current_status: The Status of the package at current_time.
        formatted_address: A str of the delivery address in the format
            used by the distance table.
    """

    current_time = None

    # __init__: O(1)
    def __init__(
        self, package_id, address, city, state, zip_code, deadline, weight
    ):
        """
        Initializes the Package with the given package information.

        Args:
            package_id: The package's unique integer ID.
            address: A str of the delivery street address.
            city: A str of the delivery city.
            state: A str of the delivery state.
            zip_code: A str of the delivery zip code.
            deadline: The latest allowable delivery time as a datetime.
            weight: A str of the package's weight in kg.
        """
        self.package_id = package_id
        self.address = address
        self.city = city
        self.state = state
        self.zip_code = zip_code
        self.deadline = deadline
        self.weight = weight
        self.timestamps = {status: None for status in Status}
        self.truck = None

    # __str__: O(1)
    def __str__(self):
        status_str = (
            f"{self.current_status.name} since {self.timestamps[self.current_status]:%I:%M %p}"
            if self.current_status
            else "UNKNOWN"
        )
        s = (
            f"{self.package_id} | {self.address}, {self.city}, {self.state} {self.zip_code}"
            f" | Weight: {self.weight} | Deadline: {self.deadline} | Status: {status_str}"
        )
        return s

    # current_status: O(n)
    @property
    def current_status(self):
        """The package's status at the current time, as indicated by the class."""
        result = None
        for status, time in self.timestamps.items():
            if time and Package.current_time >= time:
                result = status
        return result

    # formatted_address: O(1)
    @property
    def formatted_address(self):
        """The delivery address, formatted as in the delivery table."""
        return f"{self.address}\n({self.zip_code})"


class PackageList:
    """A hash table of packages."""

    # __init__: O(1)
    def __init__(self):
        """Initializes the hash table with a default size."""
        self._packages = [None] * 41

    def __str__(self):
        return "\n".join(str(package) for package in self._packages if package)

    # available_packages: O(n)
    @property
    def available_packages(self):
        """A list of packages currently in the hub."""
        return [
            pkg
            for pkg in self._packages
            if pkg is not None and pkg.current_status is Status.IN_HUB
        ]

    # delayed_packages: O(n)
    @property
    def delayed_packages(self):
        """A list of packages that are currently delayed."""
        return [
            pkg
            for pkg in self._packages
            if pkg is not None and pkg.timestamps[Status.DELAYED] is not None
        ]

    # packages_not_loaded: O(n)
    @property
    def packages_not_loaded(self):
        """A list of packages that have not been scheduled for loading."""
        return [
            pkg
            for pkg in self._packages
            if pkg is not None and pkg.timestamps[Status.LOADED] is None
        ]

    # on_time_stats: O(n)
    @property
    def on_time_stats(self):
        """A str of the ratio of packages that were delivered on time."""
        on_time_deliveries = sum(
            1
            for pkg in self._packages
            if pkg is not None
            and pkg.timestamps[Status.DELIVERED] <= pkg.deadline
        )
        total_packages = len(list(filter(None, self._packages)))
        return f"{on_time_deliveries}/{total_packages} on-time"

    # insert: O(1)
    def insert(self, package):
        """
        Inserts package into the hash table.

        The hash table uses direct hashing on the package's package_id.
        If the ID will not fit in the table as currently sized, this
        will increase the table's size to accommodate the new entry.

        Args:
            package: The package to be inserted.
        """
        if len(self._packages) <= package.package_id:
            diff = 1 + package.package_id - len(self._packages)
            self._packages.extend(None for _ in range(diff))
        self._packages[package.package_id] = package

    # remove: O(1)
    def remove(self, package_id):
        """
        Removes the package with the given ID from the table.

        If the ID is invalid or a package does not exists with the
        given ID this method does nothing.

        Args:
            package_id: The int ID of the package to be removed.
        """
        if 0 <= package_id < len(self._packages):
            self._packages[package_id] = None

    # get: O(1)
    def get(self, package_id):
        """
        Gets the package with the given ID.

        Args:
            package_id: The int ID of the requested package.

        Returns:
            The package with the given ID.

        Raises:
            KeyError: If the ID is invalid for the table or there is
                no package with the given ID.

        """
        if (
            not (0 <= package_id < len(self._packages))
            or self._packages[package_id] is None
        ):
            raise KeyError
        return self._packages[package_id]
