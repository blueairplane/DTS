# Brian Lee (brianlee3742@gmail.com)
import enum


class Status(enum.Enum):
    """
    The status of a package.

    Possible values:
        DELAYED: The package is not at the hub at the start of the day.
        IN_HUB: The package is in the hub.
        LOADED: The package has been loaded on a truck, but has not
            yet left the hub.
        EN_ROUTE: The package is out for delivery.
        DELIVERED: The package has arrived at its destination.
    """

    DELAYED = enum.auto()
    IN_HUB = enum.auto()
    LOADED = enum.auto()
    EN_ROUTE = enum.auto()
    DELIVERED = enum.auto()
