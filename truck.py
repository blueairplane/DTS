# Brian Lee (brianlee3742@gmail.com)
from status import Status


class Truck:
    """
    A single delivery truck.

    Class constants:
        MAX_PACKAGES: The maximum number of packages that can be loaded
            on a truck for a single run.
        SPEED: The average speed in mph of a delivery truck.

    Attributes:
        number: An identification number for this truck. It should be
            unique, though this is not enforced.
        delivery_runs: A list of delivery runs. Each list is itself a
            list of packages in the run, in delivery order.
        current_run: The index of the current run in the delivery_runs
            list.
        miles_travelled: A float of how many miles this truck has
            travelled in its deliveries.
        departure_times: A list of datetime objects corresponding to
            the earliest possible departure time for each of this
            truck's runs.
        finish_time: A datetime object showing when this truck
            finished all its deliveries.
        current_location: The address (formatted the same as the
            delivery table) of the truck's current location.
        current_time: A datetime object representing the current time
            as this truck makes its deliveries.
    """

    MAX_PACKAGES = 16
    SPEED = 18

    # __init__: O(1)
    def __init__(self, number):
        """
        Initializes the truck with the given ID number.

        Args:
            number: The ID number for this truck. It should be unique,
                but this is not enforced.
        """
        self.number = number
        self.delivery_runs = [[]]
        self.current_run = 0
        self.miles_travelled = 0
        self.departure_times = [None]
        self.finish_time = None
        self.current_location = "HUB"
        self.current_time = None

    # __str__: O(n)
    def __str__(self):
        s = f"Truck {self.number}"
        for run_number, run in enumerate(self.delivery_runs, start=1):
            s += (
                f"\n Run #{run_number} (Departure time: {self.departure_times[run_number - 1]:%I:%M %p})\n  "
                + "\n  ".join(str(package) for package in run)
            )
        s += f"\nTotal miles travelled: {self.miles_travelled}"
        return s

    # add_package: O(1)
    def add_package(self, new_package):
        """
        Adds new_package to the current run.

        If no more packages can be added to the current run, it adds
        a new run first, then adds the package. It also marks the
        package as loaded on this truck at the current time.

        Args:
            new_package: The package to be added to this truck's
                current delivery run.
        """
        if self.current_run_full():
            self.add_run()
        new_package.truck = self.number
        new_package.timestamps[Status.LOADED] = self.current_time
        self.delivery_runs[self.current_run].append(new_package)

    # add_run: O(1)
    def add_run(self):
        """Adds a new run to this truck's delivery runs list."""
        self.delivery_runs.append([])
        self.departure_times.append(self.current_time)
        self.current_run += 1

    # current_run_empty: O(1)
    def current_run_empty(self):
        """Returns True if the current run has no packages, False otherwise."""
        return len(self.delivery_runs[self.current_run]) == 0

    # current_run_full: O(1)
    def current_run_full(self):
        """Returns True if the current run contains the maximum allowed packages."""
        return len(self.delivery_runs[self.current_run]) == Truck.MAX_PACKAGES

    # optimize_delivery_runs: O(n²)
    def optimize_delivery_runs(self, distance_function):
        """
        Heuristically optimizes the delivery runs.

        For each run, this sorts the packages by the next-closest
        delivery destination, starting with the destination nearest
        to the hub.

        Args:
            distance_function: A function that takes two arguments,
                a start and end address, and returns the distance
                between the two locations.
        """
        for run in self.delivery_runs:
            if not run:
                self.delivery_runs.remove(run)
                self.current_run -= 1
                continue
            packages = run[:]
            run.clear()
            first_stop_index = min(
                (distance_function("HUB", pkg.formatted_address), i)
                for i, pkg in enumerate(packages)
            )[1]
            run.append(packages.pop(first_stop_index))
            while packages:
                next_closest_index = min(
                    (
                        distance_function(
                            run[-1].formatted_address, pkg.formatted_address
                        ),
                        i,
                    )
                    for i, pkg in enumerate(packages)
                )[1]
                run.append(packages.pop(next_closest_index))
        self.departure_times = [
            departure_time
            for departure_time in self.departure_times
            if departure_time
        ]
